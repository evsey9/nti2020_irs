DEBUG = True

import numpy as np
from enum import IntEnum
import math

if DEBUG:
	import cv2

class Dir(IntEnum):
	LEFT = 0
	UP = 1
	RIGHT = 2
	DOWN = 3


def det(a, b):
	return a[0] * b[1] - a[1] * b[0]


def line_intersection(line1, line2):
	xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
	ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])
	div = det(xdiff, ydiff)
	if div == 0:
		return False
	ua = ((line2[1][0] - line2[0][0]) * (line1[0][1] - line2[0][1]) - (line2[1][1] - line2[0][1]) * (line1[0][0] - line2[0][0])) / div
	ub = ((line1[1][0] - line1[0][0]) * (line1[0][1] - line2[0][1]) - (line1[1][1] - line1[0][1]) * (line1[0][0] - line2[0][0])) / div

	if ua < 0 or ua > 1 or ub < 0 or ub > 1:
		return False

	d = (det(*line1), det(*line2))
	x = det(d, xdiff) / div
	y = det(d, ydiff) / div
	return x, y

def raycast(point1, point2, lines, skip=False):
	line1 = (point1, point2)
	intersections = []
	for curline in range(len(lines)):
		point3 = (lines[curline][0], lines[curline][1])
		point4 = (lines[curline][2], lines[curline][3])
		line2 = (point3, point4)
		intersectresult = line_intersection(line1, line2)
		if intersectresult:
			intersections.append((intersectresult, lines[curline][4], curline))
	if len(intersections) > 1:
		intersections.sort(key=lambda x: np.sqrt((point1[0] - x[0][0])*(point1[0] - x[0][0]) + (point1[1] - x[0][1])*(point1[1] - x[0][1])))
	if skip:
		if len(intersections) > 1:
			return intersections[1]
		else:
			return False
	else:
		if len(intersections):
			return intersections[0]
		else:
			return False


def coord_line(dim: int, image, point1: tuple, point2: tuple, color: tuple, thickness: int):
	return cv2.line(image, (int(point1[0] * 50) + 25, dim * 50 - 25 - int(point1[1] * 50)),
						 (int(point2[0] * 50) + 25, dim * 50 - 25 - int(point2[1] * 50)),
						 color, thickness)

def hextorgb(hex):
	return tuple(int(hex[i:i + 2], 16) for i in (0, 2, 4))[::-1]


def vectorlen(vector):
	return np.sqrt(vector[0] * vector[0] + vector[1] * vector[1])


def localvector(basevec, globalvec):
	return [globalvec[0] - basevec[0], globalvec[1] - basevec[1]]


def globalvector(basevec, localvec):
	return [basevec[0] + localvec[0], basevec[1] + localvec[1]]


def normvector(vector):
	return [vector[0] / vectorlen(vector), vector[1] / vectorlen(vector)]


def rotate(x, y, xo, yo, theta): # rotate x,y around xo,yo by theta (rad)
	xr = math.cos(theta)*(x-xo)-math.sin(theta)*(y-yo) + xo
	yr = math.sin(theta)*(x-xo)+math.cos(theta)*(y-yo) + yo
	return [xr, yr]


def anglesort(vector, angle):
	#newvec = (math.cos(angle) * vector[0] - math.sin(angle) * vector[1], math.sin(angle) * vector[0] + math.cos(angle) * vector[1])
	newvec = rotate(vector[0], vector[1], 0, 0, angle)
	atn = math.atan2(newvec[1], newvec[0])
	if atn < 0:
		atn = 2 * math.pi - abs(atn)
	return atn


def angbetween(start, end, mid):
	end = end - start + math.pi * 2 if end - start < 0 else end - start
	mid = mid - start + math.pi * 2 if mid - start < 0 else mid - start
	return mid < end


def main():
	inputdata = list(map(float, input().split()))
	H = int(inputdata[0])
	N = int(inputdata[1])
	ALPHA = inputdata[2]
	H //= 250
	dirdict = {
		"L": Dir.LEFT,
		"U": Dir.UP,
		"R": Dir.RIGHT,
		"D": Dir.DOWN
	}

	inputdata = input().split()
	D = dirdict[inputdata[0]]
	Xs = int(inputdata[1]) / 250 - 0.5
	Ys = int(inputdata[2]) / 250 - 0.5
	BETA = float(inputdata[3])
	del inputdata
	if DEBUG:
		image = 255 * np.ones((H * 50, H * 60, 3), np.uint8)
		image = cv2.circle(image, (int(Xs * 50) + 25, H * 50 - 25 - int(Ys * 50)), 25, (255, 0, 0))
		if D == Dir.LEFT:
			image = cv2.arrowedLine(image, (int(Xs * 50) + 25, H * 50 - 25 - int(Ys * 50)),
									(int(Xs * 50), H * 50 - 25 - int(Ys * 50)), (255, 0, 0))
		elif D == Dir.UP:
			image = cv2.arrowedLine(image, (int(Xs * 50) + 25, H * 50 - 25 - int(Ys * 50)),
									(int(Xs * 50) + 25, H * 50 - 50 - int(Ys * 50)), (255, 0, 0))
		elif D == Dir.RIGHT:
			image = cv2.arrowedLine(image, (int(Xs * 50) + 25, H * 50 - 25 - int(Ys * 50)),
									(int(Xs * 50) + 50, H * 50 - 25 - int(Ys * 50)), (255, 0, 0))
		elif D == Dir.DOWN:
			image = cv2.arrowedLine(image, (int(Xs * 50) + 25, H * 50 - 25 - int(Ys * 50)),
									(int(Xs * 50) + 25, H * 50 - int(Ys * 50)), (255, 0, 0))
	borders = []
	for i in range(N):
		data = input().split()
		data[:4] = list(map(lambda x: int(x) / 250 - 0.5, data[:4]))
		data[0], data[1], data[2], data[3] = min(data[0], data[2]), min(data[1], data[3]), max(data[0], data[2]), max(
			data[1], data[3])
		borders.append(data)

	visiblepoints = []
	for border in borders:
		border_xlen = border[2] - border[0]
		border_ylen = border[3] - border[1]
		borderpointc = [(border[0] + border[2]) / 2, (border[1] + border[3]) / 2]
		borderpoint1 = [border.copy()[0], border.copy()[1]]
		borderpoint2 = [border.copy()[2], border.copy()[3]]
		borderpoint3 = [border.copy()[0], border.copy()[1]]
		borderpoint4 = [border.copy()[2], border.copy()[3]]
		if border_xlen:
			#print("xlen", border_xlen)
			borderpoint1[0] += 0.001
			borderpoint2[0] -= 0.001
			borderpoint3[0] -= 0.001
			borderpoint4[0] += 0.001
		if border_ylen:
			#print("ylen", border_ylen)
			borderpoint1[1] += 0.001
			borderpoint2[1] -= 0.001
			borderpoint3[1] -= 0.001
			borderpoint4[1] += 0.001
		locvec1 = localvector((Xs, Ys), borderpoint3)
		locvec1[0] *= 100
		locvec1[1] *= 100
		raycastvec1 = globalvector((Xs, Ys), locvec1)
		locvec2 = localvector((Xs, Ys), borderpoint4)
		locvec2[0] *= 100
		locvec2[1] *= 100
		raycastvec2 = globalvector((Xs, Ys), locvec2)

		res = raycast((Xs, Ys), borderpoint1, borders)
		if res:
			#image = coord_line(H, image, (Xs, Ys), (res[0][0], res[0][1]), hextorgb(res[1]), 1)
			#print(res)
			visiblepoints.append(res)

		res = raycast((Xs, Ys), borderpoint2, borders)
		if res:
			#image = coord_line(H, image, (Xs, Ys), (res[0][0], res[0][1]), hextorgb(res[1]), 1)
			#print(res)
			visiblepoints.append(res)

		res = raycast((Xs, Ys), raycastvec1, borders)
		if res:
			#image = coord_line(H, image, (Xs, Ys), (res[0][0], res[0][1]), hextorgb(res[1]), 1)
			#print(res)
			visiblepoints.append(res)

		res = raycast((Xs, Ys), raycastvec2, borders)
		if res:
			#image = coord_line(H, image, (Xs, Ys), (res[0][0], res[0][1]), hextorgb(res[1]), 1)
			#print(res)
			visiblepoints.append(res)

		#res = raycast((Xs, Ys), borderpointc, borders)
		#if res:
		#	#image = coord_line(H, image, (Xs, Ys), (res[0][0], res[0][1]), hextorgb(res[1]), 1)
		#	print(res)
		#	visiblepoints.append(res)
		if DEBUG:
			image = coord_line(H, image, (border[0], border[1]), (border[2], border[3]), hextorgb(border[4]), 2)
			image = cv2.circle(image, (int(border[0] * 50) + 25, H * 50 - 25 - int(border[1] * 50)), 3, (255, 0, 0))
			image = cv2.circle(image, (int(border[0] * 50) + 25, H * 50 - 25 - int(border[3] * 50)), 3, (255, 0, 0))
			image = cv2.putText(image, str(border[0]) + " " + str(border[1]) + " " + border[4],
								(int(border[0] * 50) + 25, H * 50 - 25 - int(border[1] * 50) - 15), 1, 1,
								color=hextorgb(border[4]))

	dirarr = [math.pi, math.pi / 2, 0, -math.pi / 2]

	leftbound = ALPHA / 2
	rightbound = -ALPHA / 2
	if D == Dir.LEFT:
		leftbound += math.pi
		rightbound += math.pi
	elif D == Dir.UP:
		leftbound += math.pi / 2
		rightbound += math.pi / 2
	elif D == Dir.RIGHT:
		leftbound += 0
		rightbound += 0
	elif D == Dir.DOWN:
		leftbound += math.pi / 2 * 3
		rightbound += math.pi / 2 * 3

	if BETA > 0:
		leftbound += BETA
	else:
		rightbound -= BETA * -1

	#leftbound = leftbound % (math.pi * 2)
	#rightbound = rightbound % (math.pi * 2)
	if DEBUG:
		print("lb:", leftbound)
		print("rb:", rightbound)
	if leftbound > rightbound:
		pass
	else:
		rightbound, leftbound = leftbound, rightbound
	endray = []
	if BETA > 0:
		endray = raycast((Xs, Ys), globalvector((Xs, Ys), (
		math.cos(leftbound - 0.001) * 10000, math.sin(leftbound - 0.001) * 10000)), borders)
	elif BETA < 0:
		endray = raycast((Xs, Ys), globalvector((Xs, Ys), (
		math.cos(rightbound + 0.001) * 10000, math.sin(rightbound + 0.001) * 10000)), borders)
	if endray:
		pass
		endray = list(endray)
		endray.append("E")
		visiblepoints.append(endray)
	else:
		if BETA > 0:
			angi = leftbound - 0.001
			while angi > rightbound + 0.001:
				endray = raycast((Xs, Ys), globalvector((Xs, Ys),(math.cos(angi) * 10000, math.sin(angi) * 10000)), borders)
				if endray:
					endray = list(endray)
					endray.append("E")
					visiblepoints.append(endray)
					break
				else:
					angi -= 0.001
		elif BETA < 0:
			angi = rightbound + 0.001
			while angi < leftbound - 0.001:
				endray = raycast((Xs, Ys), globalvector((Xs, Ys),(math.cos(angi) * 10000, math.sin(angi) * 10000)), borders)
				if endray:
					endray = list(endray)
					endray.append("E")
					visiblepoints.append(endray)
					break
				else:
					angi += 0.001
	if endray and DEBUG:
		image = coord_line(H, image, (Xs, Ys), endray[0], hextorgb('000000'), 2)
		print("e: ",endray)


	visiblepoints.sort(key=lambda x: anglesort(localvector((Xs, Ys), x[0]), -dirarr[D] + ALPHA / 2))
	if BETA < 0:
		visiblepoints.reverse()
	startray = []
	if BETA > 0:
		startray = raycast((Xs, Ys), globalvector((Xs, Ys), (math.cos(rightbound + 0.001) * 10000, math.sin(rightbound + 0.001) * 10000)), borders)
	elif BETA < 0:
		startray = raycast((Xs, Ys), globalvector((Xs, Ys), (math.cos(leftbound - 0.001) * 10000, math.sin(leftbound - 0.001) * 10000)), borders)
	if startray:
		pass
		visiblepoints.insert(0, startray)
	i = 0
	vecs = []
	if DEBUG:
		print("vecs:", vecs)
		for i in range(0, len(visiblepoints)):
			pass
			# image = coord_line(H, image, visiblepoints[i - 1][0], visiblepoints[i][0], hextorgb("000000"), 3)
			#image = coord_line(H, image, (Xs, Ys), visiblepoints[i][0], hextorgb(visiblepoints[i][1]), 1)
			#image = cv2.putText(image, str(i),
			#					(int(visiblepoints[i][0][0] * 50) + 25, H * 50 - 25 - int(visiblepoints[i][0][1] * 50) - 15), 1, 1,
			#					color=hextorgb('000000'))
	markremoval = []
	i = 0
	for i in range(len(visiblepoints)):
		if visiblepoints[i][-1] == "E":
			locvec1 = ()
			if BETA > 0:
				locvec1 = (math.cos(rightbound), math.sin(rightbound))
			else:
				locvec1 = (math.cos(leftbound), math.sin(leftbound))
			locvec2 = localvector((Xs, Ys), visiblepoints[i][0])
			normvec1 = normvector(locvec1)
			normvec2 = normvector(locvec2)
			anglebetween = np.arccos(np.dot(normvec1, normvec2))
			if abs(BETA) <= math.pi or abs(BETA) > math.pi and anglebetween > ALPHA * 2:
				visiblepoints = visiblepoints[:i + 1]
			break
	i = 0
	while i < len(visiblepoints) - 1:
		locvec1 = localvector((Xs, Ys), visiblepoints[i][0])
		locvec2 = localvector((Xs, Ys), visiblepoints[i + 1][0])
		normvec1 = normvector(locvec1)
		normvec2 = normvector(locvec2)
		anglebetween = np.arccos(np.dot(normvec1, normvec2))
		angle1 = math.atan2(normvec1[1], normvec1[0])
		angle2 = math.atan2(normvec2[1], normvec2[0])
		locvec3 = None
		normvec3 = None
		angle3 = None
		if i < len(visiblepoints) - 2:
			locvec3 = localvector((Xs, Ys) , visiblepoints[i + 2][0])
			normvec3 = normvector(locvec3)
			angle3 = math.atan2(normvec3[1], normvec3[0])
		if i < len(visiblepoints) - 2 and visiblepoints[i][1] == visiblepoints[i + 1][1] and visiblepoints[i][1] == visiblepoints[i + 2][1] and (visiblepoints[i][2] == visiblepoints[i + 1][2] or abs(angle2 - angle1) < 0.005) and (visiblepoints[i + 1][2] == visiblepoints[i + 2][2] or abs(angle3 - angle2) < 0.005):
			#visiblepoints.pop(i + 1)
			markremoval.append(i + 1)
			i += 1
		else:
			i += 1
	for i in markremoval:
		visiblepoints[i] = 0
	i = 0
	while i < len(visiblepoints):
		if visiblepoints[i] == 0:
			visiblepoints.pop(i)
		else:
			i += 1
	for i in range(len(visiblepoints) - 1):
		if visiblepoints[i][1] == visiblepoints[i + 1][1]:
			locvec1 = localvector((Xs, Ys), visiblepoints[i][0])
			locvec2 = localvector((Xs, Ys), visiblepoints[i + 1][0])
			normvec1 = normvector(locvec1)
			normvec2 = normvector(locvec2)
			anglebetween = np.arccos(np.dot(normvec1, normvec2))
			angle1 = math.atan2(normvec1[1], normvec1[0])
			angle2 = math.atan2(normvec2[1], normvec2[0])
			# if angle1 < 0:
			#	angle1 = 2 * math.pi - abs(angle1)
			# if angle2 < 0:
			#	angle2 = 2 * math.pi - abs(angle2)
			color = visiblepoints[i][1]
			if anglebetween > ALPHA / 10:
				vecs.append([angle1, angle2, color, anglebetween])
			else:
				if DEBUG:
					print("no")
	if DEBUG:
		print("vecs:", vecs)
		for i in range(0, len(visiblepoints)):
			pass
			# image = coord_line(H, image, visiblepoints[i - 1][0], visiblepoints[i][0], hextorgb("000000"), 3)
			image = coord_line(H, image, (Xs, Ys), visiblepoints[i][0], hextorgb(visiblepoints[i][1]), 1)

		# image = coord_line(H, image, visiblepoints[-1][0], visiblepoints[0][0], hextorgb("000000"), 3)
		#for i in vecs:
		#	image = coord_line(H, image, (Xs, Ys), globalvector((Xs, Ys), (math.cos(i[0]) * 10, math.sin(i[0]) * 10)), hextorgb(i[2]), 2)
		#	image = coord_line(H, image, (Xs, Ys), globalvector((Xs, Ys), (math.cos(i[1]) * 10, math.sin(i[1]) * 10)), hextorgb(i[2]), 2)
		#image = coord_line(H, image, (Xs, Ys), globalvector((Xs, Ys), (math.cos(rightbound) * 10, math.sin(rightbound) * 10)),
		#				   hextorgb('000000'), 2)
		#image = coord_line(H, image, (Xs, Ys), globalvector((Xs, Ys), (math.cos(leftbound) * 10, math.sin(leftbound) * 10)),
		#				   hextorgb('000000'), 2)

	colors = []
	for i in vecs:
		if not colors or colors[-1] != i[2]:
			colors.append(i[2])
	if len(colors) > 1:
		print(' '.join(colors))
	else:
		if len(colors):
			print(colors[0])


	if DEBUG:
		window_name = 'borders'
		cv2.imshow(window_name, image)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

if __name__ == "__main__":
	main()