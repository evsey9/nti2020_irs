from scipy import interpolate
n, m = map(int, input().split())
y_points = list(map(float, input().split()))
x_points = list(map(float, input().split()))
x1_points = list(map(float, input().split()))

coord_points = list(zip(x_points, y_points))
coord_points.sort(key=lambda x: x[0])

x_points = [i[0] for i in coord_points]
y_points = [i[1] for i in coord_points]

tck = interpolate.splrep(x_points, y_points)

y1_points = interpolate.splev(x1_points, tck)
print(' '.join(map(str, y1_points)))