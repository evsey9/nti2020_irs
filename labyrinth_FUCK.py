import numpy as np
from enum import IntEnum
import cv2
from collections import defaultdict





class Dir(IntEnum):
	LEFT = 0
	UP = 1
	RIGHT = 2
	DOWN = 3


def display_matrix(mtrx):
	for i in mtrx[::-1]:
		for j in i:
			print(j, end=" ")
		print()


def main():
	H, N, K, ALPHA = map(int, input().split())
	H //= 250
	data = input().split()
	data[1:] = map(lambda x: (int(x) - 125) // 250, data[1:])
	D, Xs, Ys, Xe, Ye = data
	del data
	borders = []
	for i in range(N):
		data = input().split()
		data[:4] = list(map(lambda x: int(x) / 250 - 0.5, data[:4]))
		data[0], data[1], data[2], data[3] = min(data[0], data[2]), min(data[1], data[3]), max(data[0], data[2]), max(data[1], data[3])
		borders.append(data)
	amovements = []
	for i in range(K):
		amovements.append(input().split())

	#print(D, Xs, Ys, Xe, Ye)
	#print(borders)
	#print(amovements)
	maze_matrix = np.ones([H, H, 4], int)
	for cell in maze_matrix[0]:
		cell[Dir.DOWN] = 0
	for cell in maze_matrix[-1]:
		cell[Dir.UP] = 0
	for row in maze_matrix:
		row[0][Dir.LEFT] = 0
		row[-1][Dir.RIGHT] = 0
	color_matrix = np.zeros([H, H, 4], "<U6")
	#display_matrix(maze_matrix)
	image = 255 * np.ones((H * 50, H * 60, 3), np.uint8)
	window_name = 'borders'

	for border in borders:  # Создать базовую матрицу цветов и столкновений
		#print(border)
		#print("border color", border[4])
		border_xlen = border[2] - border[0]
		border_ylen = border[3] - border[1]
		if border_xlen:
			if border[1] - 0.5 >= 0:
				for i in range(int(border[0] + 0.5), int(border[2] + 0.5)):
					curcell = (i, int(border[1] - 0.5))  # Мы под препятствием
					#print(curcell)
					maze_matrix[curcell[1]][curcell[0]][Dir.UP] = 0
					color_matrix[curcell[1]][curcell[0]][Dir.UP] = border[4]
			if border[1] + 0.5 < H:
				for i in range(int(border[0] + 0.5), int(border[2] + 0.5)):
					curcell = (i, int(border[1] + 0.5))  # Мы над препятствием
					#print(curcell)
					maze_matrix[curcell[1]][curcell[0]][Dir.DOWN] = 0
					color_matrix[curcell[1]][curcell[0]][Dir.DOWN] = border[4]
		if border_ylen:
			if border[0] - 0.5 >= 0:
				for i in range(int(border[1] + 0.5), int(border[3] + 0.5)):
					curcell = (int(border[0] - 0.5), i)  # Мы слева от препятствия
					#print(curcell)
					maze_matrix[curcell[1]][curcell[0]][Dir.RIGHT] = 0
					color_matrix[curcell[1]][curcell[0]][Dir.RIGHT] = border[4]
			if border[0] + 0.5 < H:
				for i in range(int(border[1] + 0.5), int(border[3] + 0.5)):
					curcell = (int(border[0] + 0.5), i)  # Мы справа от препятствия
					#print(curcell)
					maze_matrix[curcell[1]][curcell[0]][Dir.LEFT] = 0
					color_matrix[curcell[1]][curcell[0]][Dir.LEFT] = border[4]
		#display_matrix(maze_matrix)
		#display_matrix(color_matrix)
		image = cv2.line(image, (int(border[0] * 50) + 25, H * 50 - 25 - int(border[1] * 50)), (int(border[2] * 50) + 25, H * 50 - 25 - int(border[3] * 50)), tuple(int(border[4][i:i+2], 16) for i in (0, 2, 4)), 2)
		image = cv2.circle(image, (int(border[0] * 50) + 25, H * 50 - 25 - int(border[1] * 50)), 3, (255, 0, 0))
		image = cv2.circle(image, (int(border[0] * 50) + 25, H * 50 - 25 - int(border[3] * 50)), 3, (255, 0, 0))
		image = cv2.putText(image, str(border[0]) + " " + str(border[1]) + " " + border[4], (int(border[0] * 50) + 25, H * 50 - 25 - int(border[1] * 50) - 15), 1, 1, color=tuple(int(border[4][i:i+2], 16) for i in (0, 2, 4)))
	image = cv2.circle(image, (int(Xs * 50) + 25, H * 50 - 25 - int(Ys * 50)), 25, (255, 0, 0))
	#image = np.flipud(image)
	bmaze_matrix = maze_matrix.copy()
	if Xs - 1 >= 0:
		bmaze_matrix[Ys][Xs - 1][Dir.RIGHT] = 0
	if Xs + 1 < H:
		bmaze_matrix[Ys][Xs + 1][Dir.LEFT] = 0
	if Ys - 1 >= 0:
		bmaze_matrix[Ys - 1][Xs][Dir.UP] = 0
	if Ys + 1 < H:
		bmaze_matrix[Ys + 1][Xs][Dir.DOWN] = 0
	alice_matrix = np.full([H, H, 4], "E", "<U1")

	alice_dir = Dir.UP
	alice_x = 0
	alice_y = 0
	for curmovement in amovements:
		movedir = curmovement[3]
		readings = curmovement[:3] + ["E"]
		readings = list(map(lambda x: "1" if x 	== "0" else "0" if x == "1" else x, readings))
		actreadings = ["E", "E", "E", "E"]
		if alice_dir == Dir.LEFT:
			actreadings[Dir.LEFT] = readings[Dir.UP]
			actreadings[Dir.UP] = readings[Dir.RIGHT]
			actreadings[Dir.RIGHT] = readings[Dir.DOWN]
			actreadings[Dir.DOWN] = readings[Dir.LEFT]
		elif alice_dir == Dir.UP:
			actreadings[Dir.LEFT] = readings[Dir.LEFT]
			actreadings[Dir.UP] = readings[Dir.UP]
			actreadings[Dir.RIGHT] = readings[Dir.RIGHT]
			actreadings[Dir.DOWN] = readings[Dir.DOWN]
		elif alice_dir == Dir.RIGHT:
			actreadings[Dir.LEFT] = readings[Dir.DOWN]
			actreadings[Dir.UP] = readings[Dir.LEFT]
			actreadings[Dir.RIGHT] = readings[Dir.UP]
			actreadings[Dir.DOWN] = readings[Dir.RIGHT]
		elif alice_dir == Dir.DOWN:
			actreadings[Dir.LEFT] = readings[Dir.RIGHT]
			actreadings[Dir.UP] = readings[Dir.DOWN]
			actreadings[Dir.RIGHT] = readings[Dir.LEFT]
			actreadings[Dir.DOWN] = readings[Dir.UP]
		#print("am", alice_matrix[alice_y][alice_x])
		#print("act", actreadings)
		alice_matrix[alice_y][alice_x][0] = actreadings[0] if actreadings[0] != "E" else alice_matrix[alice_y][alice_x][0]
		alice_matrix[alice_y][alice_x][1] = actreadings[1] if actreadings[1] != "E" else alice_matrix[alice_y][alice_x][1]
		alice_matrix[alice_y][alice_x][2] = actreadings[2] if actreadings[2] != "E" else alice_matrix[alice_y][alice_x][2]
		alice_matrix[alice_y][alice_x][3] = actreadings[3] if actreadings[3] != "E" else alice_matrix[alice_y][alice_x][3]
		if movedir == "F":
			if alice_dir == Dir.LEFT:
				alice_x -= 1
				if alice_x < 0:
					alice_matrix = np.delete(np.hstack((np.full([H, 1, 4], "E", "<U1"), alice_matrix)), H, axis=1)
					alice_x = 0
				alice_matrix[alice_y][alice_x][Dir.RIGHT] = "1"
			elif alice_dir == Dir.UP:
				alice_y += 1
				alice_matrix[alice_y][alice_x][Dir.DOWN] = "1"
			elif alice_dir == Dir.RIGHT:
				alice_x += 1
				alice_matrix[alice_y][alice_x][Dir.LEFT] = "1"
			elif alice_dir == Dir.DOWN:
				alice_y -= 1
				if alice_y < 0:
					alice_matrix = np.delete(np.vstack((np.full([1, H, 4], "E", "<U1"), alice_matrix)), H, 0)
					alice_y = 0
				alice_matrix[alice_y][alice_x][Dir.UP] = "1"
		elif movedir == "R":
			alice_dir += 1
			if alice_dir > 3:
				alice_dir = 0
		elif movedir == "L":
			alice_dir -= 1
			if alice_dir < 0:
				alice_dir = 3
	#print("alice matrix:")
	#display_matrix(alice_matrix)
	#print("getting rid of empty rows")
	#display_matrix(np.full((H, 4), "E", "<U1"))
	for i in range(H):
		if np.array_equal(alice_matrix[H - i - 1], np.full((H, 4), "E", "<U1")):
			alice_matrix = np.delete(alice_matrix, (H - i - 1), axis=0)
	#print("newm:")
	#display_matrix(alice_matrix)
	#print("getting rid of empty columns")
	res_h = alice_matrix.shape
	#print(res_h)
	counter = 0
	for column in range(H):
		rm = True
		for row in range(res_h[0]):
			if not np.array_equal(alice_matrix[row][column], np.full((4), "E", "<U1")):
				rm = False
				break
		if rm:
			counter += 1
	for column in range(alice_matrix.shape[1])[::-1]:
		rm = True
		for row in range(alice_matrix.shape[0]):
			if not np.array_equal(alice_matrix[row][column], np.full((4), "E", "<U1")):
				rm = False
				break
		if rm:
			alice_matrix = np.delete(alice_matrix, column, axis=1)
	#alice_matrix = np.flipud(alice_matrix)
	#for row in range(alice_matrix.shape[0]):
	#	for column in range(alice_matrix.shape[1]):
	#		alice_matrix[row][column][1], alice_matrix[row][column][3] = alice_matrix[row][column][3], alice_matrix[row][column][1]
	#alice_y = alice_matrix.shape[0] - alice_y - 1
	loc_matrix = np.zeros(alice_matrix.shape[:2], "<U1")
	loc_matrix[alice_y][alice_x] = "A"
	#bmaze_matrix = np.flipud(bmaze_matrix)
	#print("axy:", alice_x, alice_y)
	#print("alice matrix:")
	#display_matrix(alice_matrix)
	#print("bob matrix:")
	#display_matrix(bmaze_matrix)
	act_offset_x = 0
	act_offset_y = 0
	found = False
	for turn in range(4):
		res_h = alice_matrix.shape
		#print("res", res_h)
		for offset_y in range(H - res_h[0] + 1):
			for offset_x in range(H - res_h[1] + 1):
				matching = True
				for row in range(res_h[0]):
					for column in range(res_h[1]):
						#print("oy:", offset_y, "ox:", offset_x, "row:", row, "column:", column)
						#print("comparing from BM", list(map(str, bmaze_matrix[offset_y + row][offset_x + column])), "with AM", alice_matrix[row][column])
						for elem in range(4):
							if alice_matrix[row][column][elem] == "E" or str(bmaze_matrix[offset_y + row][offset_x + column][elem]) == str(alice_matrix[row][column][elem]):
								pass
							else:
								matching = False
								#print("not matching(BM/AM):", bmaze_matrix[offset_y + row][offset_x + column][elem], str(alice_matrix[row][column][elem]))
								break
						if not matching:
							break
					if not matching:
						break
				if matching:
					act_offset_x = offset_x
					act_offset_y = offset_y + alice_matrix.shape[0] - 1
					#print("cur x/y: ", column, row)
					#print("foundnc at offset:")
					#print(act_offset_x, act_offset_y)
					found = True
					break
			if found:
				break
		if found:
			break
		else:
			alice_matrix = np.rot90(alice_matrix, -1)
			loc_matrix = np.rot90(loc_matrix, -1)
			for row in range(alice_matrix.shape[0]):
				for column in range(alice_matrix.shape[1]):
					alice_matrix[row][column] = np.delete(np.insert(alice_matrix[row][column], 4, alice_matrix[row][column][0]), 0)
			#alice_x, alice_y = alice_y, -alice_x
			#print("bob matrix:")
			#display_matrix(bmaze_matrix)
			#print("rotated matrix:")
			#display_matrix(alice_matrix)
	if found:
		#print("found:")
		#print(act_offset_x, act_offset_y)
		for row in range(loc_matrix.shape[0]):
			for column in range(loc_matrix.shape[1]):
				if loc_matrix[row][column] == "A":
					alice_x = act_offset_x + column
					alice_y = act_offset_y + row - loc_matrix.shape[0] + 1
		#print("axy:", alice_x, alice_y)
	else:
		#print("not found :(")
		exit(0)
	if alice_x - 1 >= 0:
		maze_matrix[alice_y][alice_x - 1][Dir.RIGHT] = 0
		if not color_matrix[alice_y][alice_x - 1][Dir.RIGHT]:
			color_matrix[alice_y][alice_x - 1][Dir.RIGHT] = '000000'
	if alice_x + 1 < H:
		maze_matrix[alice_y][alice_x + 1][Dir.LEFT] = 0
		if not color_matrix[alice_y][alice_x + 1][Dir.LEFT]:
			color_matrix[alice_y][alice_x + 1][Dir.LEFT] = '000000'
	if alice_y - 1 >= 0:
		maze_matrix[alice_y - 1][alice_x][Dir.UP] = 0
		if not color_matrix[alice_y - 1][alice_x][Dir.UP]:
			color_matrix[alice_y - 1][alice_x][Dir.UP] = '000000'
	if alice_y + 1 < H:
		maze_matrix[alice_y + 1][alice_x][Dir.DOWN] = 0
		if not color_matrix[alice_y + 1][alice_x][Dir.DOWN]:
			color_matrix[alice_y + 1][alice_x][Dir.DOWN] = '000000'
	#display_matrix(color_matrix)

	def get_cell(x, y):
		return H * y + x

	def get_coord(cell):
		return cell % H, int(cell / H)

	for row in range(color_matrix.shape[0]):
		for column in range(color_matrix.shape[1]):
			for dir in range(4):
				if color_matrix[row][column][dir]:
					if dir == Dir.LEFT:
						for column_i in range(column + 1, color_matrix.shape[1]):
							if not color_matrix[row][column_i][dir]:
								color_matrix[row][column_i][dir] = color_matrix[row][column][dir]
							else:
								break
					elif dir == Dir.UP:
						for row_i in range(row - 1, -1, -1):
							if not color_matrix[row_i][column][dir]:
								color_matrix[row_i][column][dir] = color_matrix[row][column][dir]
							else:
								break
					elif dir == Dir.RIGHT:
						for column_i in range(column - 1, -1, -1):
							if not color_matrix[row][column_i][dir]:
								color_matrix[row][column_i][dir] = color_matrix[row][column][dir]
							else:
								break
					elif dir == Dir.DOWN:
						for row_i in range(row + 1, color_matrix.shape[0]):
							if not color_matrix[row_i][column][dir]:
								color_matrix[row_i][column][dir] = color_matrix[row][column][dir]
							else:
								break
	#print("new cm:")
	#display_matrix(color_matrix)

	class Graph:
		def __init__(self, vertices):
			# No. of vertices
			self.V = vertices
			self.paths = []	
			self.shortest_path = H + 10
			# default dictionary to store graph
			self.graph = defaultdict(list)

		# function to add an edge to graph
		def addEdge(self, u, v):
			self.graph[u].append(v)


		def getAllPathsUtil(self, u, d, visited, path):

			# Mark the current node as visited and store in path
			visited[u] = True
			path.append(u)

			# If current vertex is same as destination, then add to paths
			# current path[]
			if u == d:
				self.paths.append(path.copy())
				self.shortest_path = min(self.shortest_path, len(path))
			# print(path)
			else:
				# If current vertex is not destination
				# Recur for all the vertices adjacent to this vertex
				if len(path) < self.shortest_path + 5:
					for i in self.graph[u]:
						if visited[i] == False:
							self.getAllPathsUtil(i, d, visited, path)

			# Remove current vertex from path[] and mark it as unvisited
			path.pop()
			visited[u] = False

		# Prints all paths from 's' to 'd'
		def getAllPaths(self, s, d):

			# Mark all the vertices as not visited
			visited = [False] * (self.V)

			# Create an array to store paths
			path = []
			self.paths = []
			# Call the recursive helper function to get all paths
			self.getAllPathsUtil(s, d, visited, path)
			return self.paths

	mazegraph = Graph(maze_matrix.shape[0] * maze_matrix.shape[1])
	for row in range(maze_matrix.shape[0]):
		for column in range(maze_matrix.shape[1]):
			if maze_matrix[row][column][Dir.LEFT] == 1:
				mazegraph.addEdge(get_cell(column, row), get_cell(column - 1, row))
			if maze_matrix[row][column][Dir.UP] == 1:
				mazegraph.addEdge(get_cell(column, row), get_cell(column, row + 1))
			if maze_matrix[row][column][Dir.RIGHT] == 1:
				mazegraph.addEdge(get_cell(column, row), get_cell(column + 1, row))
			if maze_matrix[row][column][Dir.DOWN] == 1:
				mazegraph.addEdge(get_cell(column, row), get_cell(column, row - 1))
	basepaths = mazegraph.getAllPaths(get_cell(Xs, Ys), get_cell(Xe, Ye))
	basepaths.sort(key=lambda x: len(x))
	#print(basepaths)
	actionpaths = []
	if D == "L":
		D = Dir.LEFT
	elif D == "U":
		D = Dir.UP
	elif D == "R":
		D = Dir.RIGHT
	elif D == "D":
		D = Dir.DOWN
	for curpath in basepaths:
		actpath = []
		curdir = D
		for i in range(1, len(curpath)):
			curcell = curpath[i]
			pathdelta = curpath[i] - curpath[i - 1]
			if pathdelta == -1:  # Going left
				if curdir == Dir.LEFT:
					actpath.append("F")
				elif curdir == Dir.UP:
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.RIGHT:
					actpath.append("L")
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.DOWN:
					actpath.append("R")
					actpath.append("F")
				curdir = Dir.LEFT
			elif pathdelta == H:  # Going up
				if curdir == Dir.UP:
					actpath.append("F")
				elif curdir == Dir.RIGHT:
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.DOWN:
					actpath.append("L")
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.LEFT:
					actpath.append("R")
					actpath.append("F")
				curdir = Dir.UP
			if pathdelta == 1:  # Going right
				if curdir == Dir.RIGHT:
					actpath.append("F")
				elif curdir == Dir.DOWN:
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.LEFT:
					actpath.append("L")
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.UP:
					actpath.append("R")
					actpath.append("F")
				curdir = Dir.RIGHT
			if pathdelta == -H:  # Going down
				if curdir == Dir.DOWN:
					actpath.append("F")
				elif curdir == Dir.LEFT:
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.UP:
					actpath.append("L")
					actpath.append("L")
					actpath.append("F")
				elif curdir == Dir.RIGHT:
					actpath.append("R")
					actpath.append("F")
				curdir = Dir.DOWN
		actionpaths.append(actpath.copy())
	actionpaths.sort(key=lambda x: len(x))
	#print(actionpaths[0])
	bob_x = Xs
	bob_y = Ys
	curdir = D
	colors = []
	if color_matrix[bob_y][bob_x][curdir]:
		colors = [color_matrix[bob_y][bob_x][curdir]]
	shortactpath = actionpaths[0]
	for act in shortactpath:
		if act == "F":
			if curdir == Dir.LEFT:
				bob_x -= 1
			elif curdir == Dir.UP:
				bob_y += 1
			elif curdir == Dir.RIGHT:
				bob_x += 1
			elif curdir == Dir.DOWN:
				bob_y -= 1
		elif act == "L":
			curdir -= 1
			if curdir < 0:
				curdir = 3
		elif act == "R":
			curdir += 1
			if curdir > 3:
				curdir = 0
		if color_matrix[bob_y][bob_x][curdir]:
			if len(colors) == 0 or color_matrix[bob_y][bob_x][curdir] != colors[-1]:
				colors.append(color_matrix[bob_y][bob_x][curdir])
	#print("output:")
	print(' '.join(colors))
	cv2.imshow(window_name, image)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

if __name__ == "__main__":
	main()
