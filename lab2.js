first = true
encoderL = brick.encoder('E3').read
encoderR = brick.encoder('E4').read
sensFront = brick.sensor('A1').read
//sensBack = brick.sensor('D2').read
sensLeft = brick.sensor('A3').read
sensRight = brick.sensor('A2').read
mL = brick.motor('M4').setPower // левый мотор
mR = brick.motor('M3').setPower // правый мотор
eL = brick.encoder('E4').read // левый энкодер
eR = brick.encoder('E3').read // правый энкодер
sF = brick.sensor('A1').read // сенсор спереди (УЗ)
sL = brick.sensor('A3').read // сенсор слева (ИК)
sR = brick.sensor('A2').read // сенсор справа (ИК)


///все рабочие куски программы
wait = script.wait

cellLength = 70
True = true
False = false

TRIK = false

robot = {
	wheelD: TRIK ? 8.2 : 5.6,
	track: TRIK ? 18.5 : 17.5,
	cpr: TRIK ? 385 : 360,
	row: 0,
	col: -1,
	azimut: 1,
	cell: -1,
	v: 60,
	number: 0
}

floor = Math.floor
round = Math.round
abs = Math.abs
sqrt = Math.sqrt
sin = Math.sin
cos = Math.cos
atan2 = Math.atan2


var xsize = 160
var ysize = 120
var arcells = 5
arr = fileRead('input.txt')

artag1 = []
artag2 = []

pos1 = arr[0].trim().split(' ')
pos2 = arr[1].trim().split(' ')

for (var i = 0; i < pos1.length; i++) {
	artag1.push(parseInt(pos1[i], 16))
	artag2.push(parseInt(pos2[i], 16))
}


A1 = Read_metka(artag1)
A2 = Read_metka(artag2)

if (A1 > 8) {
	Y = A1 % 8
	X = A2
} else if (A2 > 8) {
	Y = A2 % 8
	X = A1
}
Gyro_calibr(4000)
arrmap = localization(1, 8, 8)
mapa = arrmap[0]
//print ('arrmap:',arrmap[1],'     ', arrmap[2])
robot.cell = coord2cell(arrmap[2], arrmap[1], 8)
//print ('ROBOT CELL:',robot.cell)
//print ('Y -',Y, 'X - ',X)
finish = coord2cell(Y, X, 8)
bfs(robot.cell, finish, mapa)
brick.display().addLabel('finish', 1, 1)
brick.display().redraw()

//100% работает
//метка - 
function Read_metka(arr_pic) {
	//arr_pic=arr_pic[0].trim().split(',')
	//pic_Raw=arr_pic
	if (arr_pic.length == 1) arr_pic = arr_pic[0].trim().split(',')
	arr_pic = arr_pic.map(Number)
	pic_Raw = listToMatrix(arr_pic, xsize)
	pic_Grey = rgb24_to_grey(pic_Raw)

	picBW = grey_to_mask(pic_Grey)	//перевод изображения в черно-белое
	for (var i = 0; i < ysize; i++) {
		for (var j = 0; j < 0; j++) {
			picBW[i][j] = 0
		}
	}
	//printArr(pic_Raw, ',')
	//printArr(picBW,',')
	ABCD1 = find_corners_straight(picBW)	//поиск уголов метки 1 споосбом(строки)
	area1 = gauss(ABCD1)

	ABCD2 = find_corners_diag(picBW)	//поиск уголов метки 1 споосбом(диагонали)
	area2 = gauss(ABCD2)
	print("area1:", area1, "area2:", area2)
	if (area2 >= area1 || !area1) var ABCD = ABCD2
	if (area2 < area1 || !area2) var ABCD = ABCD1	//выбор коректного распознавания углов

	center=cross_lines(ABCD)
	//print(center)			//поиск центров клеток
	search_centers_metka(ABCD)

	dotKey = -1
	for (var i = 0; i < 4; i++) {
		if (picBW[dots_diag[i][0]][dots_diag[i][1]] == 0) {
			dotKey = i
		}
	}
	bin = make_bin(dots, dotKey)	//распознавание бинарного числа
	for (var i = 0, cnt = 0; i < bin.length; i++) cnt += bin[i] == 1 ? 1 : 0

	is_parity_check = floor(cnt % 2) == picBW[center[0]][center[1]]
	////print(floor(cnt%2)==picBW[center[0]][center[1]])
	//print('Зашифрованное число(10):',parseInt(bin.join(''),2))
	//if (is_parity_check) 
	//else //print('ошибка проверки четности')
	return parseInt(bin.join(''), 2)
}

function printArr(arr, delim, file) {
	delim = delim == undefined ? ',' : delim
	if (file != undefined) {
		script.removeFile(file)
	}

	for (var i = 0; i < arr.length; i++) {
		print(arr[i].join(delim))
	}

	if (file != undefined) {
		script.writeToFile(file, arr)
	}
}

function gauss(arr) {
	//print("newarr")
	//print("arrlen:", arr.length)
	//print("arr:", arr)
	//print("arr[3][0]: ", arr[3][0])
	var Ax, Ay, Bx, By, Cx, Cy, Dx, Dy
	if (arr.length >= 4 && arr[0] && arr[1] && arr[2] && arr[3]){
		Ax = arr[0][0]
		Ay = arr[0][1]
		Bx = arr[1][0]
		By = arr[1][1]
		Cx = arr[2][0]
		Cy = arr[2][1]
		Dx = arr[3][0]
		Dy = arr[3][1]
		area = 1 / 2 * Math.abs(Ax * By + Bx * Cy + Cx * Dy + Dx * Ay - Bx * Ay - Cx * By - Dx * Cy - Ax * Dy)// формула гауса
		//print ('Площадь фигуры: ',area)
		return area
	}
	else{
		return 0
	}
}

function quadrilateral_area(x1, y1, x2, y2, x3, y3, x4, y4) {
	return abs(x1 * y2 + x2 * y3 + x3 * y4 + x4 * y1 - x2 * y1 - x3 * y2 - x4 * y3 - x1 * y4) / 2
}

function arr1_to_arr2(arr, cols, rows)//перевод 1 строчного масива в 2 строчный
{
	var arr0ut = []
	for (var i = 0; i < rows; i++) {
		arr0ut.push([])
		for (var j = 0; j < cols; j++) {
			arr0ut[arr0ut.length - 1].push(arr[i * cols + j])
		}

	}
	return arr0ut
}

function listToMatrix(list, elementsPerSubArray) {
	var matrix = [],
		i, k
	for (i = 0, k = -1; i < list.length; i++) {
		if (i % elementsPerSubArray == 0) {
			k++
			matrix[k] = []
		}
		matrix[k].push(list[i])
	}
	return matrix
}

function fileRead(nameFile)//чтение инфы из файла
{
	return script.readAll(nameFile)
}

function Colour_to_rgb(colour)//перевод информации о цвете пикселя из RGB24 в информацию по трем каналам
{
	var R, G, B
	R = (colour & 0xFF0000) >> 16//красный
	G = (colour & 0x00FF00) >> 8 //зеленый
	B = (colour & 0x0000FF)	//синий

	return [R, G, B]
}

function rgb24_to_grey(matrix) {
	var rgbMatrix = new Array(ysize)
	for (var i = 0; i < rgbMatrix.length; i++) {
		rgbMatrix[i] = new Array(xsize)
	}

	var greyMatrix = new Array(ysize)
	for (var i = 0; i < greyMatrix.length; i++) {
		greyMatrix[i] = new Array(xsize)
	}

	for (var i = 0; i < ysize; i++) {
		for (var j = 0; j < xsize; j++) {
			var RGB = Colour_to_rgb(matrix[i][j])
			rgbMatrix[i][j] = RGB
			greyMatrix[i][j] = (RGB[0] + RGB[1] + RGB[2]) / 3
		}
	}
	return greyMatrix
}

function grey_to_mask(greyMatrix) {
	const thresholdGrey = 255 / 3
	var maskMatrix = new Array(ysize)
	for (var i = 0; i < maskMatrix.length; i++) {
		maskMatrix[i] = new Array(xsize)
	}
	for (var i = 0; i < ysize; i++) {
		for (var j = 0; j < xsize; j++) {
			maskMatrix[i][j] = greyMatrix[i][j] > thresholdGrey ? 0 : 1
		}
	}
	return maskMatrix
}

function pic_to_grey(arr) {
	var arr0ut = []
	for (var i = 0; i < arr.length; i++) {
		arr0ut.push([])
		for (var j = 0; j < arr[0].length; j++) {
			grey = Colour_to_rgb(arr[i][j])
			avg = floor((grey[0] + grey[1] + grey[2]) / 3)
			arr0ut[i].push(RGB_to_colour(avg))
		}
	}
	return arr0ut
}

function RGB_to_colour(R, G, B) {
	if (G == undefined && B == undefined) {
		G = R
		B = R
	}
	return R * Math.pow(256, 2) + G * 256 + B
}

function pic_to_BW(arr) {
	var arr0ut = []
	for (var i = 0; i < arr.length; i++) {

		arr0ut.push([])
		for (var j = 0; j < arr[0].length; j++) {
			bw = 1
			////print(Colour_to_rgb(arr[i][j]))
			//if(Colour_to_rgb(arr[i][j])[0]>=255/5)
			if (arr[i][j] > 16777214 / 3) {
				bw = 0
			}
			arr0ut[i].push(bw)
		}
	}
	return arr0ut

}

function find_corners(arr, delta) {

	//print("Поиск по прямым:")
	delta = delta == undefined ? 3 : delta
	var A = [], B = [], C = [], D = []
	var picH = arr.length - delta
	var picW = arr[0].length - delta


	forA:
	for (var i = delta; i < picH; i++) {
		for (var j = delta; j < picW; j++) {
			if (arr[i][j] == 1) {
				A = [i, j]
				break forA
			}
		}

	}
	//print("точка А= ",A)

	forB:
	for (var i = picW - 1; i >= delta; i--) {
		for (var j = delta; j < picH; j++) {
			if (arr[j][i] == 1) {
				B = [j, i]
				break forB
			}
		}
	}
	//print("точка Б= ", B)

	forC:
	for (var i = picH - delta; i > delta; i--) {
		for (var j = picW - delta; j > delta; j--) {
			if (arr[i][j] == 1) {
				C = [i, j]
				break forC
			}
		}

	}
	//print("точка C= ",C)


	forD:
	for (var i = delta; i <= picW; i++) {
		for (var j = picH - delta; j >= delta; j--) {
			if (arr[j][i] == 1) {
				D = [j, i]
				break forD
			}
		}
	}
	//print("точка D= ",D)
	print("A:", A, "B:", B, "C:", C, "D:", D)
	return [A, B, C, D]
}

function find_corners2(arrPic, delta) {
	print("arrpic Y:", arrPic.length, "arrpic X:", arrPic[0].length)
	delta = delta == undefined ? 3 : delta
	var A = [], B = [], C = [], D = []
	var x, y
	forA:
	for (var r = delta; r < arrPic.length; r++) {
		for (c = delta; c <= r; c++) {
			x = c + delta
			y = r - c + delta
			if (x < ysize && y < xsize && arrPic[x][y] == 1) {
				A = [x, y]
				break forA
			}
		}
	}
	forB:
	for (var r = delta; r < arrPic.length - delta; r++) {
		var z = arrPic[0].length - 1 - r
		for (var x = delta; x <= r; x++) {
			y = z
			////print (x,'  ',y)
			if (x < ysize && y < xsize && arrPic[x][y] == 1) {
				B = [x, y]
				break forB
			}
		}
	}
	forD:
	for (var r = delta; r < arrPic.length - delta; r++) {
		var z = arrPic.length - 1 - r
		for (var c = delta; c <= r; c++ , z++ , y++) {
			var x = z
			var y = c
			////print(x , '  ', y)
			if (x < ysize && y < xsize && arrPic[x][y] == 1) {
				D = [x, y]
				break forD
			}
		}
	}

	forC:
	for (var r = delta; r < arrPic.length - delta; r++) {
		var z = arrPic[0].length - 1 - r // y
		var l = arrPic.length - 1 - r // x
		for (var c = arrPic.length - 1 - r; c <= arrPic.length - 1 - delta; c++) {
			y = z
			if (x < ysize && y < xsize && arrPic[c][y] == 1) {
				C = [c, y]
				break forC
			}
			z--
		}
	}
	return [A, B, C, D]
}

function find_corners_straight(maskMatrix, delta) {
	delta = delta || 0
	var ULcorner = [0, 0]
	var URcorner = [0, 0]
	var DLcorner = [0, 0]
	var DRcorner = [0, 0]

	//upleft corner
	var x = delta
	var y = delta
	while (maskMatrix[y][x] != 1) {
		//print(x + " " + y)
		if (x >= xsize - 1 - delta)
			y += 1, x = delta
		else
			x += 1
	}
	ULcorner = [y, x]

	//upright corner
	var x = xsize - 1 - delta
	var y = delta
	while (maskMatrix[y][x] != 1) {
		if (y >= ysize - 1 - delta)
			x -= 1, y = delta
		else
			y += 1
	}
	URcorner = [y, x]

	//downleft corner
	var x = delta
	var y = delta
	while (maskMatrix[y][x] != 1) {
		if (y >= ysize - 1 - delta)
			x += 1, y = delta
		else
			y += 1
	}
	DLcorner = [y, x]

	//downright corner
	var x = delta
	var y = ysize - 1 - delta
	while (maskMatrix[y][x] != 1) {
		if (x >= xsize - 1 - delta)
			y -= 1, x = delta
		else
			x += 1
	}
	DRcorner = [y, x]
	return [ULcorner, URcorner, DRcorner, DLcorner]
}

function find_corners_diag(maskMatrix, delta) {
	delta = delta || 0
	var ULcorner = [0, 0]
	var URcorner = [0, 0]
	var DLcorner = [0, 0]
	var DRcorner = [0, 0]
	//upleft corner
	var x = delta
	var y = delta
	var l = delta
	while (maskMatrix[y][x] != 1) {
		if (y >= ysize - 1 - delta)
			l += 1, y = delta, x = l
		if (x <= delta)
			l += 1, y = delta, x = l
		else
			y += 1, x -= 1
	}
	ULcorner = [y, x]
	print("ULcorner", ULcorner)
	//upright corner
	var x = xsize - 1 - delta
	var y = delta
	var l = delta
	while (maskMatrix[y][x] != 1) {
		if (y >= ysize - 1 - delta) {
			l += 1, y = delta, x = xsize - 1 - delta - l
			continue
		}
		if (x >= xsize - 1 - delta)
			l += 1, y = delta, x = xsize - 1 - delta - l
		else
			y += 1, x += 1
	}
	URcorner = [y, x]
	print("URcorner", URcorner)
	//down left corner
	var x = delta
	var y = ysize - 1 - delta
	var l = delta
	while (maskMatrix[y][x] != 1) {
		if (y <= 1 + delta) {
			l += 1, y = ysize - 1 - delta, x = l
			continue
		}
		if (x <= delta)
			l += 1, y = ysize - 1 - delta, x = l
		else
			y -= 1, x -= 1
	}
	DLcorner = [y, x]
	print("DLcorner", DLcorner)
	//down right corner
	var x = xsize - 1 - delta
	var y = ysize - 1 - delta
	var l = delta
	while (maskMatrix[y][x] != 1) {
		if (y <= 1 + delta) {
			l += 1, y = ysize - 1 - delta, x = xsize - 1 - l - delta
			continue
		}
		if (x >= xsize - 1 - delta)
			l += 1, y = ysize - 1 - delta, x = xsize - 1 - delta - l
		else
			y -= 1, x += 1
	}
	DRcorner = [y, x]
	print("DRcorner", DRcorner)
	return [ULcorner, URcorner, DRcorner, DLcorner]
}


function cross_lines(abcd) {
	var x1, x2, x3, x4, y1, y2, y3, y4, x, y
	print("abcd:", abcd)
	x1 = abcd[0][0]
	y1 = abcd[0][1]
	x2 = abcd[2][0]
	y2 = abcd[2][1]
	x3 = abcd[1][0]
	y3 = abcd[1][1]
	x4 = abcd[3][0]
	y4 = abcd[3][1]
	//printArr(abcd)

	x = -(((x1 * y2 - x2 * y1) * (x4 - x3) - (x3 * y4 - x4 * y3) * (x2 - x1)) / ((y1 - y2) * (x4 - x3) - (y3 - y4) * (x2 - x1)));
	y = ((y3 - y4) * (-x) - (x3 * y4 - x4 * y3)) / (x4 - x3);

	//print('Центр метки: ', floor(x)," ",floor(y))
	return [floor(x), floor(y)]

}

function split_line(start, end, parts) {
	var results = [start], dX, dY, x, y
	dX = (end[0] - start[0]) / parts
	dY = (end[1] - start[1]) / parts
	for (var i = 0; i < floor(parts); i++) {
		x = Math.round(results[i][0] + dX)
		y = Math.round(results[i][1] + dY)
		results.push([x, y])
	}
	results.push(end)
	return results
}

function search_centers_metka(abcd) {
	dots = []
	dots_diag = []
	center = cross_lines(abcd)
	for (var i = 0; i < 4; i++) {
		dots_diag.push(split_line(center, abcd[i], 2.5)[1])
	}
	for (var i = 0; i < 4; i++) {
		var idX = (i + 1) % 4
		dots.push(split_line(dots_diag[i], dots_diag[idX], 2)[1])
	}
	//print("Точки по диагоналям: ",dots_diag)
	//print("Точки по прямым: ",dots)

}



function make_bin(dots, key)//НАДО ФИКСИТЬ
{
	var bin = []
	if (key == 0) {
		bin.push(picBW[dots[2][0]][dots[2][1]])
		bin.push(picBW[dots[1][0]][dots[1][1]])
		bin.push(picBW[dots[3][0]][dots[3][1]])
		bin.push(picBW[dots[0][0]][dots[0][1]])
		//print("Обнаруженна метка типа 2")
	}
	if (key == 1) {
		bin.push(picBW[dots[3][0]][dots[3][1]])
		bin.push(picBW[dots[2][0]][dots[2][1]])
		bin.push(picBW[dots[0][0]][dots[0][1]])
		bin.push(picBW[dots[1][0]][dots[1][1]])
		//print("Обнаруженна метка типа 3")	
	}
	if (key == 2) {
		bin.push(picBW[dots[0][0]][dots[0][1]])
		bin.push(picBW[dots[3][0]][dots[3][1]])
		bin.push(picBW[dots[1][0]][dots[1][1]])
		bin.push(picBW[dots[2][0]][dots[2][1]])
		//print("Обнаруженна метка типа 4")	
	}
	if (key == 3) {
		bin.push(picBW[dots[1][0]][dots[1][1]])
		bin.push(picBW[dots[0][0]][dots[0][1]])
		bin.push(picBW[dots[2][0]][dots[2][1]])
		bin.push(picBW[dots[3][0]][dots[3][1]])
		//print("Обнаруженна метка типа 1")	
	}
	print ("Зашифованное число(2): ",bin)
	return bin
}

function Proverka_dostovernosti(arr1)	//проверка данных на достоверность(отсеивание ложных срабатываний)
{
	arr = []
	arr = arr1.trim().split("")
	number_of_correct = 0
	number_status = 0
	status_arr = arr[0]
	for (var i = 1; i <= arr.length; i++) {
		now_status = arr[i]
		if (status_arr != now_status) {
			if (number_status < 10) {
				number_status = number_status + 1
			}
			else {
				status_arr = now_status
				number_status = 0
				if (now_status == 1) number_of_correct = number_of_correct + 1
			}
		}
		else {
			number_status = 0
		}
	}
	//print('Количество верных считываний: ',number_of_correct)	
}

//хрень, которая работает не всегда и не в 100% случаев

function localization(azimut, rows, cols) { //0 5 5, 0 y x
	cols = cols || rows
	var map = []          //3 - стена, 1 - нет стены, 1 - были, 2 - видели, но не были, -1 - неизвестно и / или не доступно
	for (var i = 0; i < rows * 4 - 3; i++) {
		map.push([])
		for (var i1 = 0; i1 < cols * 4 - 3; i1++) {
			map[i].push(0)
		}
	}
	function coord2cell(x, y, MapSizeX) {
		return ((x) / 2 + (y) * MapSizeX / 2)
	}
	function cell2coord(cell, MapSizeX) {
		var row = Math.floor(cell / MapSizeX)
		var col = (cell - row * MapSizeX)
		return [col * 2, row * 2]
	}
	////print(matrix.length, ' ', matrix[0].length)
	////print(map.length, ' ', map[0].length)
	robot.azimut = azimut % 4
	switch (robot.azimut) {
		case -1: Gyro = [0, 90, 180, -90]; break;
		case 0: Gyro = [0, 90, 180, -90]; break;
		case 1: Gyro = [-90, 0, 90, 180]; break;
		case 2: Gyro = [180, -90, 0, 90]; break;
		case 3: Gyro = [90, 180, -90, 0]; break;
	}
	var path = []
	var x = (cols - 1) * 2, y = (rows - 1) * 2, mapSizeX = (map[0].length + 1) / 2, mapSizeY = map.length, cell = coord2cell(x, y, mapSizeX), cellMax = coord2cell(map.length - 1, map[0].length - 1, mapSizeX) + 1
	function bfs2(start, sizeX) {
		function nearCells(cell) {
			var cells = []
			var coords = cell2coord(cell, mapSizeX)
			try {
				if (map[coords[1] - 1][coords[0]] == 1)
					cells.push(coord2cell(coords[0], coords[1] - 2, mapSizeX))
			}
			catch (err) {
				wait(1)
			}
			try {
				if (map[coords[1]][coords[0] + 1] == 1)
					cells.push(coord2cell(coords[0] + 2, coords[1], mapSizeX))
			}
			catch (err) {
				wait(1)
			}
			try {
				if (map[coords[1] + 1][coords[0]] == 1)
					cells.push(coord2cell(coords[0], coords[1] + 2, mapSizeX))
			}
			catch (err) {
				wait(1)
			}
			try {
				if (map[coords[1]][coords[0] - 1] == 1)
					cells.push(coord2cell(coords[0] - 2, coords[1], mapSizeX))
			}
			catch (err) {
				wait(1)
			}
			////print(cells)
			return cells
		}
		function move_actions(cells, sizeX) {
			var actions = []
			var az = robot.azimut
			for (var i = 0; i < cells.length; i++) {
				var direction = cells[i + 1] - cells[i]
				if (direction == 1) {
					if (az == 0)
						actions.push('R')
					else if (az == 2)
						actions.push('L')
					else if (az == 3)
						actions.push('LL')
					else actions.push('F')
					az = 1
				}
				if (direction == -1) {
					if (az == 0)
						actions.push('L')
					else if (az == 2)
						actions.push('R')
					else if (az == 1)
						actions.push('LL')
					else actions.push('F')
					az = 3
				}
				if (direction == sizeX) {
					if (az == 0)
						actions.push('LL')
					else if (az == 1)
						actions.push('R')
					else if (az == 3)
						actions.push('L')
					else actions.push('F')
					az = 2
				}
				if (direction == -sizeX) {
					if (az == 1)
						actions.push('L')
					else if (az == 2)
						actions.push('LL')
					else if (az == 3)
						actions.push('R')
					else actions.push('F')
					az = 0
				}
			}
			//print(actions)
			return actions
		}
		function drive_actions(actions) {
			for (var i = 0; i < actions.length; i++) {
				if (actions[i] == 'F') {
					forvard_gyro_inc(cellLength, 70)
					switch (robot.azimut) {
						case 0: robot.cell -= 8; break;
						case 1: robot.cell += 1; break;
						case 2: robot.cell += 8; break;
						case 3: robot.cell -= 1; break;
					}
					//print(robot.cell)
				}
				else {
					forvard_gyro_inc(cellLength / 6, 70)
					if (actions[i] == 'R')
						gyro_smart((robot.azimut + 1) % 4)
					else if (actions[i] == 'L')
						gyro_smart((robot.azimut + 3) % 4)
					else gyro_smart((robot.azimut + 2) % 4)
					forvard_gyro_inc(cellLength * 5 / 6, 70)
				}
			}
		}
		var queue = [start]
		var p = 0
		var visited = []
		var path = []
		for (var i = 0; i < cells; i++) visited.push(0)
		forA:
		while (queue.length > 0) {
			p = queue.shift()
			path.push(p)
			var arr = nearCells(p)
			for (var i = 0; i < arr.length; i++) {
				if (path.indexOf(arr[i]) == -1)
					queue.push(arr[i])
				if (map[cell2coord(arr[i], mapSizeX)[1]][cell2coord(arr[i], mapSizeX)[0]] == 2) {
					path.push(arr[i])
					break forA
				}
			}
		}
		////print(path)
		if (map[cell2coord(path.slice(-1)[0], mapSizeX)[1]][cell2coord(path.slice(-1)[0], mapSizeX)[0]] != 2)
			return -1
		var path_back = [path.slice(-1)[0]]
		path.reverse()
		for (var i = 1; i < path.length; i++) {
			if (nearCells(path_back.slice(-1)).indexOf(path[i]) != -1) { //map[path_back.slice(-1)][path[i]] == 1
				path_back.push(path[i])
			}
		}
		path_back.reverse()
		//print(path_back)

		drive_actions(move_actions(path_back, sizeX))
		return path[0]
	}
	function updateMap(cell1, cell2, value, az) {
		////print(cell1, ' ', cell2)
		var coords1 = cell2coord(cell1, mapSizeX)
		var coords2 = cell2coord(cell2, mapSizeX)
		////print(coords1, ' ', coords2, '\n')
		switch (az) {
			case 0:
				if (value == 0)
					map[coords2[1] + 1][coords2[0]] = 3
				else {
					map[coords2[1] + 1][coords2[0]] = 1
					map[coords2[1]][coords2[0]] = map[coords2[1]][coords2[0]] == 1 ? 1 : 2
				}
				break
			case 1:
				if (value == 0)
					map[coords2[1]][coords2[0] - 1] = 3
				else {
					map[coords2[1]][coords2[0] - 1] = 1
					map[coords2[1]][coords2[0]] = map[coords2[1]][coords2[0]] == 1 ? 1 : 2
				}
				break
			case 2:
				if (value == 0)
					map[coords2[1] - 1][coords2[0]] = 3
				else {
					map[coords2[1] - 1][coords2[0]] = 1
					map[coords2[1]][coords2[0]] = map[coords2[1]][coords2[0]] == 1 ? 1 : 2
				}
				break
			case 3:
				if (value == 0)
					map[coords2[1]][coords2[0] + 1] = 3
				else {
					map[coords2[1]][coords2[0] + 1] = 1
					map[coords2[1]][coords2[0]] = map[coords2[1]][coords2[0]] == 1 ? 1 : 2
				}
				break
		}
		if (value == 0)
			return 0
		else
			return map[coords2[1]][coords2[0]]
	}
	while (true) {
		var state = readSensors()
		var cells = [cell - mapSizeX, cell + 1, cell + mapSizeX, cell - 1]
		////print(cell, ' ', cell2coord(cell, mapSizeX), ' ', mapSizeX, ' ', robot.azimut, ' ', cellMax)
		map[(cell2coord(cell, mapSizeX))[1]][(cell2coord(cell, mapSizeX))[0]] = 1
		var path1 = [0, 0, 0, 0]
		if (cell - mapSizeX >= 0) path1[(4 - robot.azimut) % 4] = updateMap(cell, cell - mapSizeX, state[(4 - robot.azimut) % 4], 0)
		if (cell + 1 < cellMax) path1[(5 - robot.azimut) % 4] = updateMap(cell, cell + 1, state[(5 - robot.azimut) % 4], 1)
		if (cell + mapSizeX < cellMax) path1[(6 - robot.azimut) % 4] = updateMap(cell, cell + mapSizeX, state[(6 - robot.azimut) % 4], 2)
		if (cell - 1 >= 0) path1[(7 - robot.azimut) % 4] = updateMap(cell, cell - 1, state[(7 - robot.azimut) % 4], 3)

		//print(path1)
		wait(100)
		/*for (i in map){
			//print(map[i])
		}*/
		wait(100)

		if (path1.indexOf(2) != -1) {
			if (path1[0] != 2) {
				forvard_gyro_inc(cellLength / 6)
				if (path1[1] == 2) gyro_smart(robot.azimut + 1)
				else if (path1[3] == 2) gyro_smart(robot.azimut + 3)
				else if (path1[2] == 2) gyro_smart(robot.azimut + 2)
				forvard_gyro_inc(cellLength / 6 * 5)
				cell = cells[robot.azimut]
				wait(200)
			}
			else {
				forvard_gyro_inc(cellLength)
				cell = cells[robot.azimut]
				wait(200)
			}
		}
		else {
			var k = bfs2(cell, mapSizeX)
			if (k == -1)
				break
			else {
				cell = k
				////print(k)
				T = true
				wait(200)
				//break
			}

		}
	} /**/

	x = 0, y = 0
	forA:
	for (y = 0; y < map.length; y += 2) {
		for (x = 0; x < map[y].length; x += 2) {
			if (map[y][x] == 1)
				break forA
		}
	}
	//print([x, y])
	x1 = cell2coord(cell, mapSizeX)[0] - x
	y1 = cell2coord(cell, mapSizeX)[1] - y
	var matrix = []
	for (var i = 0; i < rows * cols; i++) {
		matrix.push([])
		for (var i1 = 0; i1 < rows * cols; i1++)
			matrix[i].push(0)
	}

	for (var i = 0; i < matrix.length; i++) {
		var x0 = x + i % cols * 2, y0 = y + Math.floor(i / rows) * 2
		var cellNow = coord2cell(x + i % cols * 2, y + Math.floor(i / rows) * 2)
		////print(x0, ' ', y0, ' ', i)
		if (i - 8 > 0) {
			matrix[i][i - cols] = map[y + Math.floor(i / rows) * 2 - 1][x + i % cols * 2] == 3 ? 0 : 1
			matrix[i - cols][i] = map[y + Math.floor(i / rows) * 2 - 1][x + i % cols * 2] == 3 ? 0 : 1
		}
		if (i + 1 < matrix[0].length) {
			matrix[i][i + 1] = map[y + Math.floor(i / rows) * 2][x + i % cols * 2 + 1] == 3 ? 0 : 1
			matrix[i + 1][i] = map[y + Math.floor(i / rows) * 2][x + i % cols * 2 + 1] == 3 ? 0 : 1
		}
		if (i + 8 < matrix[0].length) {
			matrix[i][i + cols] = map[y + Math.floor(i / rows) * 2 + 1][x + i % cols * 2] == 3 ? 0 : 1
			matrix[i + cols][i] = map[y + Math.floor(i / rows) * 2 + 1][x + i % cols * 2] == 3 ? 0 : 1
		}
		if (i - 1 > 0) {
			matrix[i][i - 1] = map[y + Math.floor(i / rows) * 2][x + i % cols * 2 - 1] == 3 ? 0 : 1
			matrix[i - 1][i] = map[y + Math.floor(i / rows) * 2][x + i % cols * 2 - 1] == 3 ? 0 : 1
		}
	}
	for (var i in matrix)
		//print(matrix[i])
		return ([matrix, x1 / 2, y1 / 2])
}
function Gyro_calibr(timee) {
	brick.gyroscope().calibrate(timee)
	wait(timee + 500)
	Gyro = [0, 90, 180, -90]
	switch (robot.azimut) {
		case -1: Gyro = [0, 90, 180, -90]; break;
		case 0: Gyro = [0, 90, 180, -90]; break;
		case 1: Gyro = [-90, 0, 90, 180]; break;
		case 2: Gyro = [180, -90, 0, 90]; break;
		case 3: Gyro = [90, 180, -90, 0]; break;
	}
}
function readSensors() {
	var sens = [0, 0, 1, 0]

	sens[0] = sensFront() > cellLength ? 1 : 0
	sens[1] = sensRight() > cellLength ? 1 : 0
	sens[3] = sensLeft() > cellLength ? 1 : 0
	if ((first) && ((sens[0] != 0) || (sens[1] != 0) || (sens[3] != 0))) {
		sens[2] = 0
		first = false
	}
	return sens.join('')
}
function forvard_gyro_inc(cm, v) {
	v = v == undefined ? robot.v : v

	//start = start == undefined ? sensRight() : start
	var path0 = encoderL()
	var sgn = sign(cm)
	var path = cm2cpr(cm) + path0
	var v0 = 25, vM = v0
	var errL = encoderL(), errR = encoderR()
	var startStop = path / 4
	var dV = (v - v0) / 20
	var gyro = (Gyro[robot.azimut] * 1000)
	var l1 = 20
	var l2 = 10
	var val = []
	var val1 = []
	var val2 = []
	var vallAll = []
	var bobool = false
	if (gyro == 180000)
		bobool = true

	while (val.length < 5) {
		val.push(sensFront())
		val1.push(-((encoderL() - errL) - (encoderR() - errR)))
		var a = bobool ? (getYaw() < 0 ? 360000 + getYaw() : getYaw()) : getYaw()
		val2.push(-(gyro - a))

		wait(10)
	}
	while ((filterMedian(val) > l2) && (encoderL() <= path)) {
		if (filterMedian(val) >= l1) {
			if (vM < v) { vM += dV }
			//else if (encoderL() > path0 + startStop*3) {vM -= dV}}
		}
		else {
			vM = (v - v0) / (l1 - filterMedian(val) + 1) + v0
		}

		err1 = filterMedian(val1) * 0.004
		err2 = filterMedian(val2) * 0.012
		u1 = (err1 + err2) / 3
		u1 = u1 * u1 * 0.02 * sign(u1) + u1 * 0.2
		if (vallAll.length < 4) {
			//u1 = u
		}
		else {
			//u1 = filterMedian(vallAll)
		}

		vL = (vM - u1) * sgn
		vR = (vM + u1) * sgn
		////print(err1, ' ', err2, ' ', u1, ' ', vL, ' ', vR, ' ', v, sgn, val2, filterMedian(val2))
		motors(vL, vR)
		wait(5)
		//val.push(sensFront())
		////print(val)
		//if (vallAll.length > 8)
		//vallAll.slice(1)
		//vallAll.push(u)
		val = val.slice(1)
		val.push(sensFront())
		val2 = val2.slice(1)
		var a = bobool ? (getYaw() < 0 ? 360000 + getYaw() : getYaw()) : getYaw()
		val2.push(-(gyro - a))
		////print(bobool, ' ', a, ' ', gyro, ' ', val2)
		val1 = val1.slice(1)
		val1.push(-((encoderL() - errL) - (encoderR() - errR)))
	}
	motors(0)

}
function getYaw() { return brick.gyroscope().read()[6] }

function sign(num) { return num >= 0 ? 1 : -1 }

function cm2cpr(cm) { return (cm / (Math.PI * robot.wheelD)) * robot.cpr }

function filterMedian(arr) {

	var arr1 = arr.slice(0).sort(function (a, b) { return a - b })
	if (arr1.length % 2 == 0)
		return Math.floor((arr1[arr1.length / 2] + arr1[arr1.length / 2 - 1]) / 2)
	else
		return Math.floor(arr1[(arr1.length - 1) / 2])
}
function motors(mL, mR) {
	mR = mR == undefined ? mL : mR
	brick.motor('M4').setPower(mL)
	brick.motor('M3').setPower(mR)
}

function gyro_smart(angle, v, t) {
	if (angle != robot.azimut) {
		v = v == undefined ? robot.v : v
		var vMin = 40
		if (v < vMin)
			v = 40
		var k = v
		var bobool = false
		var mz
		//var a = bobool ? (getYaw() < 0 ? 360000 + getYaw() : getYaw()) : getYaw()
		angle %= 4

		if ((robot.azimut - angle + 4) % 4 == 1)
			mz = 1
		else
			mz = -1

		angle1 = angle
		if (Gyro[angle] * 1000 == 180000)
			bobool = true
		angle = Gyro[angle] * 1000
		var bobool1 = false
		/*
		if ((getYaw() > 135000) || (getYaw() < -135000)) {
			mz = ((getYaw() < 0 ? 360000 + getYaw() : getYaw()) - angle - 180000) > 0 ? 1 : -1
		}
		else {
			mz = getYaw() - angle > 0 ? -1 : 1
		}
		*/


		/*if ((bobool) && (robot.azimut == 3))
			mz = -1
		if (mz == -1)
			bobool1 = true
		*/

		if ((getYaw() > 160000) || (getYaw() < -160000)) {
			if (mz == 1)
				while ((getYaw() > 160000) || (getYaw() < -160000)) {
					motors(-v, v)
					wait(10)
				}
			else
				while ((getYaw() > 160000) || (getYaw() < -160000)) {
					motors(v, -v)
					wait(10)
				}
		}

		var yaw = angle - (bobool ? (getYaw() < 0 ? 360000 + getYaw() : getYaw()) : getYaw())
		function yaww() {
			return (bobool ? (getYaw() < 0 ? 360000 + getYaw() : getYaw()) : getYaw())
		}
		//print((yaww() - angle) / 1000)
		wait(1000)
		while ((Math.abs(yaw) > 2000)) {
			yaw = yaww() - angle
			var k = yaw / 1000
			if (Math.abs(yaw) < 45000)
				sp = (Math.abs(yaw) / 45000) * (v - vMin) + vMin
			else
				sp = v
			/*if (bobool1) {
				if (yaw > 0) {
					k = -k
				}
			}
			else
				if (yaw < 0) {
					k = -k
				}
		}
		else
			k = v
		
		//motors(k * mz, -k * mz)
		//print(k * mz, -k * mz)
		wait(100)
		////print(Math.abs(angle - getYaw()) / 1000)
		yaw = angle - (bobool ? (getYaw() < 0 ? 360000 + getYaw() : getYaw()) : getYaw())/**/
			////print(k)
			if (k < 0)
				motors(sp, -sp)
			else
				motors(-sp, sp)

			wait(10)
		}
		robot.azimut = angle1
		motors(0)
		wait(100)
	}
}
function cell2coord(cell, mapRows) {
	var rows = mapRows || MapRows
	var row = Math.floor(cell / rows)
	var col = cell - row * rows
	return [row, col]

}

function coord2cell(row, col, cols) {
	cols = cols || MapCols
	return row * cols + col
}
function bfs(start, end, mtrx) {
	mtrx = mtrx || map
	var queue = [start]
	var visited = []
	var path = []
	for (var i = 0; i < mtrx.length; i++) visited.push(0)
	while (queue.length > 0) {
		var p = queue.shift()
		if (visited[p] == 0) {
			visited[p] = 1
			path.push(p)
			for (var i = 0; i < mtrx.length; i++) {
				if (mtrx[p][i] == 1 && visited[i] == 0)
					queue.push(i)
			}
		}
		if (p == end) break
	}
	var path_back = [Number(path.slice(-1))]
	path.reverse()
	for (var i = 1; i < path.length; i++) {
		if (mtrx[path_back.slice(-1)][path[i]] == 1)
			path_back.push(path[i])
	}
	path_back.reverse()
	//return path_back
	drive_actions(move_actions(path_back, 8))
}
function move_actions(cells, sizeX) {
	sizeX = sizeX != undefined ? sizeX : 5
	var actions = []
	var az = robot.azimut
	for (var i = 0; i < cells.length; i++) {
		var direction = cells[i + 1] - cells[i]
		if (direction == 1) {
			if (az == 0)
				actions.push('R')
			else if (az == 2)
				actions.push('L')
			else if (az == 3)
				actions.push('LL')
			else actions.push('F')
			az = 1
		}
		if (direction == -1) {
			if (az == 0)
				actions.push('L')
			else if (az == 2)
				actions.push('R')
			else if (az == 1)
				actions.push('LL')
			else actions.push('F')
			az = 3
		}
		if (direction == sizeX) {
			if (az == 0)
				actions.push('LL')
			else if (az == 1)
				actions.push('R')
			else if (az == 3)
				actions.push('L')
			else actions.push('F')
			az = 2
		}
		if (direction == -sizeX) {
			if (az == 1)
				actions.push('L')
			else if (az == 2)
				actions.push('LL')
			else if (az == 3)
				actions.push('R')
			else actions.push('F')
			az = 0
		}
	}
	//print(actions)
	return actions
}
function drive_actions(actions) {
	for (var i = 0; i < actions.length; i++) {
		if (actions[i] == 'F') {
			forvard_gyro_inc(cellLength, 70)
			switch (robot.azimut) {
				case 0: robot.cell -= 8; break;
				case 1: robot.cell += 1; break;
				case 2: robot.cell += 8; break;
				case 3: robot.cell -= 1; break;
			}
			//print(robot.cell)
		}
		else {
			forvard_gyro_inc(cellLength / 6, 70)
			if (actions[i] == 'R')
				gyro_smart((robot.azimut + 1) % 4)
			else if (actions[i] == 'L')
				gyro_smart((robot.azimut + 3) % 4)
			else gyro_smart((robot.azimut + 2) % 4)
			forvard_gyro_inc(cellLength * 5 / 6, 70)
		}
	}
}