var __interpretation_started_timestamp__;
var pi = 3.141592653589793;
robot = {
	v: 80,
	d: 5.6,
	track: 17.5,
	cpr: 360,
}
var readGyro = brick.gyroscope().read
mL = brick.motor('M4').setPower // левый мотор
mR = brick.motor('M3').setPower // правый мотор
eL = brick.encoder('E4').read // левый энкодер
eR = brick.encoder('E3').read // правый энкодер
sF = brick.sensor('D1').read // сенсор спереди (УЗ)
sL = brick.sensor('D2').read // сенсор слева (ИК)


var eLeft = brick.encoder(E4)
var eRight = brick.encoder(E3)
sign = function (n) {
	return n > 0 ? 1 : n = 0 ? 0 : -1
}

function motors(vl, vr) {
	mL(vl == undefined ? robot.v : vl)
	mR(vr == undefined ? robot.v : vr)
}
function getYaw() {
	yawValue = readGyro()[6]
	if (yawValue > 180000) {
		yawValue = -yawValue + (yawValue - 180000) * 2
	}
	return yawValue
}

wait = script.wait
var main = function()
{
	__interpretation_started_timestamp__ = Date.now()
	v = 80
	var v0 = 80,
		vM = v0
	//regulator values
	var encd_kP = 0.0
	var gyro_kP = -3.5
	var encLst = eL()
	var encRst = eR()
	var gyrost = 0
	var control = 0
	var encdControl = 0
	var gyroControl = 0
	readings = [sL()]
	encReadings = []
	lastReading = sL()
	while(sF() > 25){
		if(Math.abs(sL() - lastReading) > 0){
			readings.push(sL())
			encReadings.push((eL() + eR()) / 2)
			lastReading = sL()
		}
		vM = Math.min(Math.max(v0, vM), robot.v)
		var cur_yaw = getYaw()
		encdControl = ((eL() - encLst) - (eR() - encRst)) * encd_kP
		gyroControl = (gyrost - Math.abs(cur_yaw) / 1000) * gyro_kP * sign(cur_yaw)

		//print("gyrocontrol" + gyroControl)
		//print("encdcontrol" + encdControl)
		//print("yaw " + getYaw() / 1000)
		//brick.display().addLabel(control, 1, 1)
		control = encdControl + gyroControl
		//brick.display().addLabel("motor speeds: " + (vM - control) + " " + (vM + control) , 1, 20)
		//brick.display().redraw()
		//brick.display().addLabel(getYaw() / 1000, 1, 1)
		//brick.display().redraw()
		motors(Math.max(vM - control, -100), Math.max(vM + control, -100))
		wait(10)
	}
	motors(0, 0)
	wait(500)
	maxReading = Math.max.apply(null, readings)
	nEnc = encReadings[readings.indexOf(maxReading)]
	print(readings)
	print(maxReading)
	while (Math.abs((eL() + eR()) / 2 - nEnc) > 72) {
		vM = Math.min(Math.max(v0, vM), robot.v)
		var cur_yaw = getYaw()
		encdControl = ((eL() - encLst) - (eR() - encRst)) * encd_kP
		gyroControl = (gyrost - Math.abs(cur_yaw) / 1000) * gyro_kP * sign(cur_yaw)

		//print("gyrocontrol" + gyroControl)
		//print("encdcontrol" + encdControl)
		//print("yaw " + getYaw() / 1000)
		//brick.display().addLabel(control, 1, 1)
		control = encdControl + gyroControl
		//brick.display().addLabel("motor speeds: " + (vM - control) + " " + (vM + control) , 1, 20)
		//brick.display().redraw()
		//brick.display().addLabel(getYaw() / 1000, 1, 1)
		//brick.display().redraw()
		motors(Math.max(-vM - control, -100), Math.max(-vM + control, -100))
		wait(10)
	}
	motors(0, 0)
	wait(500)
	motors(-100, 100)
	wait(400)
	motors(100, 100)
	wait(4000)
	motors(0, 0)
	brick.display().addLabel("finish", 1, 1)
	brick.display().redraw()
	wait(5000)
	return;
}
