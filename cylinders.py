MAX = 3700.0
n = int(input())
enc = []
s = []
for i in range(n):
	data = list(map(float, input().split()))
	enc.append(data[0])
	s.append(data[1])

rising = False
cylinders = []
cylinder = 0
cur_cylinder = 0
cur_measurements = 0
on_cylinder = False
delta = 0
for i in range(n):
	if not on_cylinder:
		if s[i] < MAX:
			on_cylinder = True
			cylinder += 1
	if on_cylinder:
		if rising:
			if s[i] > s[i - 1]:
				rising = False
		if s[i] >= MAX or i == n - 1:
			cur_measurements += i == n - 1
			if cur_measurements >= 3:
				cylinders.append(cur_cylinder)
			on_cylinder = False
			cur_cylinder = 0
			cur_measurements = 0
		elif i > 0 and not rising and s[i] < s[i - 1]:
			rising = True
			if cur_measurements >= 3:
				cylinders.append(cur_cylinder)
			cur_cylinder = 0
			cur_measurements = 0
		else:
			cur_measurements += 1
			if i > 0:
				delta = enc[i] - enc[i - 1]
				cur_cylinder += delta

print(cylinders.index(max(cylinders)) + 1)
